package com.ruoyi.demo.service;

import java.util.List;
import com.ruoyi.demo.domain.DemoCustomer;

/**
 * 客户主表(mb)Service接口
 * 
 * @author 数据小王子
 * 2023-07-11
 */
public interface IDemoCustomerService 
{
    /**
     * 查询客户主表(mb)
     * 
     * @param customerId 客户主表(mb)主键
     * @return 客户主表(mb)
     */
    DemoCustomer selectDemoCustomerByCustomerId(Long customerId);

    /**
     * 查询客户主表(mb)列表
     * 
     * @param demoCustomer 客户主表(mb)
     * @return 客户主表(mb)集合
     */
    List<DemoCustomer> selectDemoCustomerList(DemoCustomer demoCustomer);

    /**
     * 新增客户主表(mb)
     * 
     * @param demoCustomer 客户主表(mb)
     * @return 结果
     */
    int insertDemoCustomer(DemoCustomer demoCustomer);

    /**
     * 修改客户主表(mb)
     * 
     * @param demoCustomer 客户主表(mb)
     * @return 结果
     */
    int updateDemoCustomer(DemoCustomer demoCustomer);

    /**
     * 批量删除客户主表(mb)
     * 
     * @param customerIds 需要删除的客户主表(mb)主键集合
     * @return 结果
     */
    int deleteDemoCustomerByCustomerIds(Long[] customerIds);

    /**
     * 删除客户主表(mb)信息
     * 
     * @param customerId 客户主表(mb)主键
     * @return 结果
     */
    int deleteDemoCustomerByCustomerId(Long customerId);
}
