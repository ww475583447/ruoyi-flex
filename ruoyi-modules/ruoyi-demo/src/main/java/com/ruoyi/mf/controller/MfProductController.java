package com.ruoyi.mf.controller;

import java.util.ArrayList;
import java.util.List;
import lombok.RequiredArgsConstructor;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.ruoyi.common.excel.core.ExcelResult;
import com.ruoyi.common.core.core.domain.R;
import com.ruoyi.common.excel.utils.ExcelUtil;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.web.annotation.RepeatSubmit;
import com.ruoyi.common.web.core.BaseController;
import jakarta.annotation.Resource;
import com.ruoyi.mf.domain.vo.MfProductVo;
import com.ruoyi.mf.domain.vo.MfProductImportVo;
import com.ruoyi.mf.domain.bo.MfProductBo;
import com.ruoyi.mf.listener.MfProductImportListener;
import com.ruoyi.mf.service.IMfProductService;
import org.springframework.web.multipart.MultipartFile;


/**
 * 产品树Controller
 *
 * @author 数据小王子
 * 2024-04-12
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/mf/product")
public class MfProductController extends BaseController
{
    @Resource
    private IMfProductService mfProductService;

    /**
     * 查询产品树列表
     */
    @SaCheckPermission("mf:product:list")
    @GetMapping("/list")
    public R<List<MfProductVo>> list(MfProductBo mfProductBo)
    {
        List<MfProductVo> list = mfProductService.selectList(mfProductBo);
        return R.ok(list);
    }

    /**
     * 导出产品树列表
     */
    @SaCheckPermission("mf:product:export")
    @Log(title = "产品树", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MfProductBo mfProductBo)
    {
        List<MfProductVo> list = mfProductService.selectList(mfProductBo);
        ExcelUtil.exportExcel(list, "产品树", MfProductVo.class, response);
    }

    /**
     * 导入数据
     *
     * @param file          导入文件
     * @param updateSupport 是否更新已存在数据
     */
    @Log(title = "产品树", businessType = BusinessType.IMPORT)
    @SaCheckPermission("mf:product:import")
    @PostMapping("/importData")
    public R<Void> importData(MultipartFile file, boolean updateSupport) throws Exception {
        ExcelResult<MfProductImportVo> result = ExcelUtil.importExcel(file.getInputStream(), MfProductImportVo.class, new MfProductImportListener(updateSupport));
        return R.ok(result.getAnalysis());
    }

    @SaCheckPermission("mf:product:import")
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) {
        ExcelUtil.exportExcel(new ArrayList<>(), "产品树", MfProductImportVo.class, response);
    }

    /**
     * 获取产品树详细信息
     */
    @SaCheckPermission("mf:product:query")
    @GetMapping(value = "/{productId}")
    public R<MfProductVo> getInfo(@PathVariable Long productId)
    {
        return R.ok(mfProductService.selectById(productId));
    }

    /**
     * 新增产品树
     */
    @SaCheckPermission("mf:product:add")
    @Log(title = "产品树", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping
    public R<Void> add(@Validated @RequestBody MfProductBo mfProductBo)
    {
        boolean inserted = mfProductService.insert(mfProductBo);
        if (!inserted) {
            return R.fail("新增产品树记录失败！");
        }
        return R.ok();
    }

    /**
     * 修改产品树
     */
    @SaCheckPermission("mf:product:edit")
    @Log(title = "产品树", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping
    public R<Void> edit(@Validated @RequestBody MfProductBo mfProductBo)
    {
        Boolean updated = mfProductService.update(mfProductBo);
        if (!updated) {
            return R.fail("修改产品树记录失败!");
        }
        return R.ok();
    }

    /**
     * 删除产品树
     */
    @SaCheckPermission("mf:product:remove")
    @Log(title = "产品树", businessType = BusinessType.DELETE)
    @DeleteMapping("/{productIds}")
    public R<Void> remove(@PathVariable Long[] productIds)
    {
        boolean deleted = mfProductService.deleteByIds(productIds);
        if (!deleted) {
            return R.fail("删除产品树记录失败!");
        }
        return R.ok();
    }
}
